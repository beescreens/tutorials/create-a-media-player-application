# Create a Media Player application tutorial

This repository is part of the [Create a Media Player application](https://docs.beescreens.ch/tutorials/create-a-media-player-application/introduction/) tutorial. For all details, check the tutorial.
