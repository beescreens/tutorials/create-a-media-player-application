/*
  Warnings:

  - Added the required column `slideshow_id` to the `slides` table without a default value. This is not possible if the table is not empty.

*/
-- CreateTable
CREATE TABLE "slideshows" (
    "id" TEXT NOT NULL PRIMARY KEY
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_slides" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "slideshow_id" TEXT NOT NULL,
    "src" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "alt" TEXT NOT NULL,
    "interval" INTEGER,
    CONSTRAINT "slides_slideshow_id_fkey" FOREIGN KEY ("slideshow_id") REFERENCES "slideshows" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_slides" ("alt", "id", "interval", "src", "type") SELECT "alt", "id", "interval", "src", "type" FROM "slides";
DROP TABLE "slides";
ALTER TABLE "new_slides" RENAME TO "slides";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
