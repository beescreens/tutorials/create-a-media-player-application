import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { SlideshowsModule } from './slideshows/slideshows.module';

@Module({
  imports: [SlideshowsModule],
  controllers: [AppController],
})
export class AppModule {}
