import {
    Get,
    Controller,
    Param,
    Post,
    Body,
    Patch,
    Delete,
    NotFoundException,
    HttpCode,
} from '@nestjs/common';
import { SlideshowsService } from './slideshows.service';
import { CreateSlideshow } from './types/create-slideshow.type';
import { UpdateSlideshow } from './types/update-slideshow.type';

@Controller('api/slideshows')
export class SlideshowsApiController {
    constructor(private readonly slideshowsService: SlideshowsService) { }

    @Get()
    async getSlideshowsApi() {
        const slideshows = await this.slideshowsService.getSlideshows();

        return slideshows;
    }

    @Get(':id')
    async getSlideshowApi(@Param('id') id: string) {
        const slideshow = await this.slideshowsService.getSlideshow(id);

        if (!slideshow) {
            throw new NotFoundException();
        }

        return slideshow;
    }

    @Post()
    async createSlideshowApi(@Body() createSlideshow: CreateSlideshow) {
        const newSlideshow = await this.slideshowsService.createSlideshow(createSlideshow);

        return newSlideshow;
    }

    @Patch(':id')
    async updateSlideshowApi(@Param('id') id: string, @Body() updateSlideshow: UpdateSlideshow) {
        try {
            const updatedSlideshow = await this.slideshowsService.updateSlideshow(id, updateSlideshow);

            return updatedSlideshow;
        } catch (error) {
            throw new NotFoundException();
        }
    }

    @Delete(':id')
    @HttpCode(204)
    async deleteSlideshowApi(@Param('id') id: string) {
        try {
            await this.slideshowsService.deleteSlideshow(id);
        } catch (error) {
            throw new NotFoundException();
        }
    }
}
