import { Module } from '@nestjs/common';
import { PrismaModule } from '../prisma/prisma.module';
import { SlideshowsApiController } from './slideshows-api.controller';
import { SlideshowsViewsController } from './slideshows-views.controller';
import { SlideshowsService } from './slideshows.service';

@Module({
  imports: [PrismaModule],
  controllers: [SlideshowsApiController, SlideshowsViewsController],
  providers: [SlideshowsService],
})
export class SlideshowsModule {}
