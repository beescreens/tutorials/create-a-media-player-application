import {
    Get,
    Controller,
    Render,
    Param,
    Res,
} from '@nestjs/common';
import { Response } from 'express';
import { SlideshowsService } from './slideshows.service';

@Controller('/slideshows')
export class SlideshowsViewsController {
    constructor(private readonly slideshowsService: SlideshowsService) { }

    @Get()
    @Render('slideshows/index')
    async getSlideshows() {
        const slideshows = await this.slideshowsService.getSlideshows();

        return { slideshows: slideshows };
    }

    @Get(':id')
    async getSlideshow(@Res() res: Response, @Param('id') id: string) {
        const slideshow = await this.slideshowsService.getSlideshow(id);

        if (!slideshow) {
            return res.redirect('/slideshows');
        }

        return res.render(
            'slideshows/[id]',
            { slideshow: slideshow },
        );
    }
}
