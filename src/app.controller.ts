import { Get, Controller, Redirect } from '@nestjs/common';

@Controller()
export class AppController {
  @Get()
  @Redirect('/slideshows')
  root() { }
}
